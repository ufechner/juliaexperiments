module publisher
using ZMQ
using asset_system
using ProtoBuf

export Publisher, publishEstSysState, close

println("Testing with ZMQ version $(ZMQ.version)")

type Publisher
    context
    socket
    state
    time::Float64
    counter::Int
    event_counter::Int
    function Publisher()
        context = Context(1)
        socket  = Socket(context, PUB)
        ZMQ.bind(socket, "tcp://*:5575")
        sleep(0.5)
        println("Created publisher on port 5575!")
        state = EstimatedSystemState()
        state.header=StateHeader()
        set_field(state, :header, StateHeader())
        set_field(state, :heading_d, Heading_D())
        set_field(state, :velocity_w, Velocity_W())
        set_field(state, :course_d, Course_D())
        time = 0.0
        counter = 0
        event_counter = 0
        new(context, socket, state, time, counter, event_counter)
    end
end

function close(pub::Publisher)
    ZMQ.close(pub.socket)
    ZMQ.close(pub.context)
    println("Conncection closed!")
end

function publishEstSysState(pub, pos, velocity_w, v_app_norm, avg_winddirection_ground, elevation, azimuth,
                            length, heading, course, heading_d, course_d, print = false)
    set_field(pub.state.header, :time_rel, pub.time)
    set_field(pub.state, :pos_east, convert(Float32, pos[1]))
    set_field(pub.state, :pos_north, convert(Float32, pos[2]))
    set_field(pub.state, :height, convert(Float32, pos[3]))
    set_field(pub.state.velocity_w, :vx, convert(Float32, velocity_w[1]))
    set_field(pub.state.velocity_w, :vy, convert(Float32, velocity_w[2]))
    set_field(pub.state.velocity_w, :vz, convert(Float32, velocity_w[3]))
    set_field(pub.state, :apparent_windvelocity_kite, convert(Float32, v_app_norm))
    set_field(pub.state, :avg_winddirection_ground, convert(Float32, avg_winddirection_ground))
    set_field(pub.state, :elevation, convert(Float32, elevation))
    set_field(pub.state, :azimuth, convert(Float32, azimuth))
    set_field(pub.state, :tether_length_reelout, convert(Float32, length))
    set_field(pub.state, :heading, convert(Float32, heading))
    set_field(pub.state, :course, convert(Float32, course))
    set_field(pub.state.heading_d, :hx, convert(Float32, heading_d[1]))
    set_field(pub.state.heading_d, :hy, convert(Float32, heading_d[2]))
    set_field(pub.state.course_d, :cx, convert(Float32, course_d[1]))
    set_field(pub.state.course_d, :cy, convert(Float32, course_d[2]))

    iob = PipeBuffer();
    write(iob, char(14))
    writeproto(iob, pub.state)
    if print
        println(bytes2hex(iob.data))
        println(string(pub.state))
        println("Size in bytes: ", iob.size)
        @assert iob.size == 107
    end
    ZMQ.send(pub.socket, Message(iob.data))
    # ZMQ.send(pub.socket, Message(iob)) # does not work !
end

end

