using publisher
using asset_system

function testPublisher(pub, print = false)
    pos = [1., 2., 3.]
    velocity_w = [8., 0.1, 0.]
    v_app_norm = 20.0
    avg_winddirection_ground = 0.2
    elevation = 0.5
    azimuth = 0.4
    length = 150.0
    heading = 20.0
    course = 22.0
    heading_d = [3.,4.]
    course_d  = [4., 5.]
    publishEstSysState(pub, pos, velocity_w, v_app_norm, avg_winddirection_ground, elevation, azimuth,
                       length, heading, course, heading_d, course_d, print)
end

pub = publisher.Publisher()
testPublisher(pub, true)
tic()
for i = 1:10000
    testPublisher(pub)
end
toc()
publisher.close(pub)
