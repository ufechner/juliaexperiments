SRC_DIR=$HOME/00QT-Software/00AssetLib/ProtoBuf
DST_DIR=$HOME/00PythonSoftware/KiteSim/Julia/asset
protoc -I=$SRC_DIR --julia_out=$DST_DIR $SRC_DIR/asset.pod.proto 
protoc -I=$SRC_DIR --julia_out=$DST_DIR $SRC_DIR/asset.winch.proto
protoc -I=$SRC_DIR --julia_out=$DST_DIR $SRC_DIR/asset.system.proto
protoc -I=$SRC_DIR --julia_out=$DST_DIR $SRC_DIR/asset.bug.proto
