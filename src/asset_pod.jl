module asset_pod


using ProtoBuf
import ProtoBuf.meta

type __enum_PodCtrlState
    REMOTE_CONTROL::Int32
    SAFETY_MODE::Int32
    JOYSTICK_MODE::Int32
    NO_CONNECTION::Int32
    JOYSTICK_NO_REMOTE::Int32
    __enum_PodCtrlState() = new(0,1,2,3,4)
end #type __enum_PodCtrlState
const PodCtrlState = __enum_PodCtrlState()

type __enum_PodCommand
    READ_POD_STATE::Int32
    READ_SETTINGS::Int32
    WRITE_SETTINGS::Int32
    WRITE_EL_SETTINGS::Int32
    NEW_CTRL_CMD::Int32
    READ_ELECTRUM_STATE::Int32
    __enum_PodCommand() = new(0,1,2,3,4,5)
end #type __enum_PodCommand
const PodCommand = __enum_PodCommand()

type __enum_GPS_State
    GPS_NOFIX::Int32
    GPS_FIX::Int32
    GPS_FIX2D::Int32
    GPS_FIX3D::Int32
    __enum_GPS_State() = new(0,1,2,3)
end #type __enum_GPS_State
const GPS_State = __enum_GPS_State()

type __enum_ComStatePath
    ROUTER_ROUTER::Int32
    ROUTER_UART::Int32
    UART_ROUTER::Int32
    UART_UART::Int32
    __enum_ComStatePath() = new(0,1,2,3)
end #type __enum_ComStatePath
const ComStatePath = __enum_ComStatePath()

type __enum_KALMAN_State
    KALMAN_INVALID::Int32
    KALMAN_VALID::Int32
    __enum_KALMAN_State() = new(0,10)
end #type __enum_KALMAN_State
const KALMAN_State = __enum_KALMAN_State()

type PositionECEF
    latitude::Float32
    longitude::Float32
    altitude::Float32
    PositionECEF() = (o=new(); fillunset(o); o)
end #type PositionECEF

type OrientationEX
    phi::Float32
    theta::Float32
    psi::Float32
    OrientationEX() = (o=new(); fillunset(o); o)
end #type OrientationEX
meta(t::Type{OrientationEX}) = meta(t, Symbol[:phi,:theta,:psi], Int[], Dict{Symbol,Any}())

type V_EX
    vx::Float32
    vy::Float32
    vz::Float32
    V_EX() = (o=new(); fillunset(o); o)
end #type V_EX
meta(t::Type{V_EX}) = meta(t, Symbol[:vx,:vy,:vz], Int[], Dict{Symbol,Any}())

type V_ET
    vh::Float32
    ch::Float32
    vz::Float32
    isValid::Int32
    isDoppler::Int32
    V_ET() = (o=new(); fillunset(o); o)
end #type V_ET
meta(t::Type{V_ET}) = meta(t, Symbol[:vh,:ch,:vz], Int[], Dict{Symbol,Any}())

type PosError
    sigma_north::Float32
    sigma_east::Float32
    sigma_up::Float32
    PosError() = (o=new(); fillunset(o); o)
end #type PosError
meta(t::Type{PosError}) = meta(t, Symbol[:sigma_north,:sigma_east,:sigma_up], Int[], Dict{Symbol,Any}())

type AV_KS
    p::Float32
    q::Float32
    r::Float32
    AV_KS() = (o=new(); fillunset(o); o)
end #type AV_KS
meta(t::Type{AV_KS}) = meta(t, Symbol[:p,:q,:r], Int[], Dict{Symbol,Any}())

type Acceleration_KS
    ax::Float32
    ay::Float32
    az::Float32
    Acceleration_KS() = (o=new(); fillunset(o); o)
end #type Acceleration_KS

type KiteState
    pos::PositionECEF
    orientation::OrientationEX
    velocity::V_EX
    angular_velocity::AV_KS
    acc::Acceleration_KS
    gps_state::Int32
    kalman_state::Int32
    pos_trimble::PositionECEF
    velocity_trimble::V_ET
    pos_error_trimble::PosError
    gps_state_trimble::Int32
    gps_sat_trimble::Int32
    glos_sat_trimble::Int32
    time_trimble::Float64
    delay_trimble::Float64
    delay_trimble_time::Float64
    KiteState() = (o=new(); fillunset(o); o)
end #type KiteState

type KiteCtrlState
    time::Float64
    steering::Int32
    depower::Int32
    KiteCtrlState() = (o=new(); fillunset(o); o)
end #type KiteCtrlState
meta(t::Type{KiteCtrlState}) = meta(t, Symbol[], Int[3,1,2], Dict{Symbol,Any}())

type PodSettings
    min_depower::Int32
    max_depower::Int32
    min_steering::Int32
    max_steering::Int32
    min_brake::Int32
    max_brake::Int32
    full_steering::Float32
    full_depower::Float32
    full_power::Float32
    max_motor_temp::Int32
    max_gearbox_temp::Int32
    max_controller_temp::Int32
    battery_capacity::Float32
    weight::Float32
    PodSettings() = (o=new(); fillunset(o); o)
end #type PodSettings

type PodTemperatures
    t_smotor::Int32
    t_dmotor::Int32
    t_sgearbox::Int32
    t_dgearbox::Int32
    t_scontroller::Int32
    t_dcontroller::Int32
    t_ASTI_CPU::Int32
    t_BRAINY_CPU::Int32
    t_BAT_01::Int32
    t_BAT_02::Int32
    PodTemperatures() = (o=new(); fillunset(o); o)
end #type PodTemperatures

type PodElectrics
    time::Float64
    i_max_steering::Float32
    i_max_depower::Float32
    i_av_steering::Float32
    i_av_depower::Float32
    i_eff_steering::Float32
    i_eff_depower::Float32
    u_av_batt::Float32
    u_min_batt::Float32
    u_av_depower::Float32
    u_min_depower::Float32
    u_av_steering::Float32
    u_min_steering::Float32
    PodElectrics() = (o=new(); fillunset(o); o)
end #type PodElectrics

type PodSensors
    time::Float64
    windspeed::Float32
    force::Float32
    u_force::Int32
    u_wind::Float32
    abs_pressure::Int32
    PodSensors() = (o=new(); fillunset(o); o)
end #type PodSensors

type ElectrumSettings
    tick_time_ms::Int32
    log_multiplier::Int32
    ElectrumSettings() = (o=new(); fillunset(o); o)
end #type ElectrumSettings
meta(t::Type{ElectrumSettings}) = meta(t, Symbol[:tick_time_ms], Int[], Dict{Symbol,Any}())

type ElectrumState
    jitter_ms::Float32
    min_jitter_ms::Float32
    max_jitter_ms::Float32
    disk_space::Int32
    mem_space::Int32
    cpu_load::Float32
    ElectrumState() = (o=new(); fillunset(o); o)
end #type ElectrumState

type ComState
    package_no::Int32
    time_sent::Float64
    router_packets_received::Int32
    router_packets_sent::Int32
    laird_packets_received::Int32
    laird_packets_sent::Int32
    path::Int32
    ComState() = (o=new(); fillunset(o); o)
end #type ComState

type PodState
    counter::Int32
    acknowledge::Int32
    kite_state::KiteState
    kite_ctrl_state::KiteCtrlState
    pod_ctrl::Int32
    pod_temp::PodTemperatures
    pod_el::PodElectrics
    pod_sensors::PodSensors
    electrum_state::ElectrumState
    com_state::ComState
    time::Float64
    time_sent::Float64
    time_received::Float64
    crc::Int32
    time_clock::Float64
    watchdog_active::Bool
    PodState() = (o=new(); fillunset(o); o)
end #type PodState
meta(t::Type{PodState}) = meta(t, Symbol[:counter], Int[], Dict{Symbol,Any}())

type KiteCtrlCmd
    cmd_steering::Int32
    cmd_depower::Int32
    reset_mc::Bool
    KiteCtrlCmd() = (o=new(); fillunset(o); o)
end #type KiteCtrlCmd

type PodCtrl
    counter::Int32
    pod_cmd::Int32
    kite_cmd::KiteCtrlCmd
    electrum_settings::ElectrumSettings
    time::Float64
    time_sent::Float64
    time_received::Float64
    crc::Int32
    time_clock::Float64
    reset_counter::Bool
    pod_settings::PodSettings
    PodCtrl() = (o=new(); fillunset(o); o)
end #type PodCtrl
meta(t::Type{PodCtrl}) = meta(t, Symbol[:counter], Int[], [:pod_cmd => PodCommand.READ_POD_STATE])

type BrainyLog
    pod_state::PodState
    pod_ctrl::PodCtrl
    pod_sensors::PodSensors
    BrainyLog() = (o=new(); fillunset(o); o)
end #type BrainyLog

type Kite
    weight::Float32
    ld_max::Float32
    ld_min::Float32
    size::Float32
    max_force::Float32
    Kite() = (o=new(); fillunset(o); o)
end #type Kite
meta(t::Type{Kite}) = meta(t, Symbol[], Int[], [:weight => 11,:ld_max => 5,:ld_min => 1,:size => 25,:max_force => 4905])

export PodCtrlState, PodCommand, GPS_State, ComStatePath, KALMAN_State, PositionECEF, OrientationEX, V_EX, V_ET, PosError, AV_KS, Acceleration_KS, KiteState, KiteCtrlState, PodSettings, PodTemperatures, PodElectrics, PodSensors, ElectrumSettings, ElectrumState, ComState, PodState, KiteCtrlCmd, PodCtrl, BrainyLog, Kite
export meta
end # module asset_pod
