#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" This module provides the class Publisher for publishing ParticleSystem, EstimatedSystemState
and Event messages on port 5575. """
import zmq
import time as ti
import sys
sys.path.append('/home/ufechner/00PythonSoftware/KiteSim')
import asset.system_pb2 as system
from math import pi
import numpy as np
from Timer import Timer

SEGMENTS = 6

# pylint: disable=E1101
class Publisher(object):
    """Publisher for ZeroMQ messages. Is publishing on port ParticleSystem messages
       and EstimatedSystemState messages on port 5575. """
    def __init__(self):
        self.context = zmq.Context()
        self.sock = self.context.socket(zmq.PUB)
        self.sock.bind('tcp://*:5575')
        ti.sleep(0.5) # wait until the connection is established before sending any messages
        self.state = system.EstimatedSystemState()
        self.time = 0.0
        self.counter = 0
        self.event_counter = 0

    def publishEstSysState(self, pos, velocity_w, v_app_norm, avg_winddirection_ground, elevation, azimuth, \
                                 length, heading, course, heading_d, course_d):
        """ Publish the estimated system state (for FrontView etc.) """
        self.state.Clear()
        self.state.header.time_rel = self.time
        self.state.pos_east = pos[0]
        self.state.pos_north = pos[1]
        self.state.height = pos[2]
        self.state.velocity_w.vx = velocity_w[0]
        self.state.velocity_w.vy = velocity_w[1]
        self.state.velocity_w.vz = velocity_w[2]
        self.state.apparent_windvelocity_kite = v_app_norm
        self.state.avg_winddirection_ground = avg_winddirection_ground
        self.state.elevation = elevation
        self.state.azimuth = azimuth
        self.state.tether_length_reelout = length
        self.state.heading = heading
        self.state.course = course
        self.state.heading_d.hx, self.state.heading_d.hy = heading_d
        self.state.course_d.cx, self.state.course_d.cy = course_d
        binstr = chr(14) + self.state.SerializeToString()
        # print self.state
        # print ''.join(x.encode('hex') for x in binstr)

        self.sock.send(binstr)

    def getEvent(self, event):
        m_event = system.EventMsg()
        m_event.counter = self.event_counter
        m_event.time = ti.time()
        m_event.event = event
        if event==system.FAST_CLK:
            m_event.event_source = system.CLOCK
        else:
            m_event.event_source = system.SYSTEM_OPERATOR
        self.event_counter += 1
        return m_event

    def sendMessage(self, message, msg_type):
        binstr = chr(msg_type) + message.SerializeToString()
        #print binstr.encode('hex_codec')
        self.sock.send(binstr)

    def sendEvent(self, event):
        self.sendMessage(self.getEvent(event), system.mtEventMsg)

    def close(self):
        self.sock.close()

if __name__ == "__main__":
    print "\nPublishing 10 EstimatedSystemState messages on port 5575..."
    pub = Publisher()
    pos = np.array((1., 2., 3.))
    velocity_w = np.array((8., 0.1, 0.))
    v_app_norm = 20.0
    avg_winddirection_ground = 0.2
    elevation = 0.5
    azimuth = 0.4
    length = 150.0
    heading = 20.0
    course = 22.0
    heading_d = (3.,4.)
    course_d = (4., 5.)
    with Timer() as t1:
        for i in range(10000):
            pub.publishEstSysState(pos, velocity_w, v_app_norm, avg_winddirection_ground, elevation, azimuth, \
                           length, heading, course, heading_d, course_d)
    print "time for publishEstSysState  [s]:   ", (t1.secs)  #/ 10000 * 1e6


