using publisher
using asset_system

function debug_string(var, type_)
    println(var)
end

function testEnum()
    event = asset_system.Event.CYCLE_START
    println(debug_string(event, asset_system.Event))
end

testEnum()
