To publish protobuf encoded messages over ZeroMQ run:

julia TestZeroMQLink.jl

To compare with Python run:

python TestZeroMQLink.py

On my computer, a core i7-2600 I got the following performance:

Julia:  270 ms
Python: 750 ms

for sending 10000 messages of a size of 107 bytes each.

Uwe Fechner, 9.3.2014
