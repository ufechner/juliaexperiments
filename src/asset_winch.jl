module asset_winch


using ProtoBuf
import ProtoBuf.meta

type __enum_WinchCtrlState
    OFF::Int32
    SETUP::Int32
    ERROR::Int32
    MANUAL::Int32
    POINT_SET::Int32
    AUTO_REELOUT::Int32
    AUTO_REELIN::Int32
    __enum_WinchCtrlState() = new(0,1,2,3,4,5,6)
end #type __enum_WinchCtrlState
const WinchCtrlState = __enum_WinchCtrlState()

type __enum_WinchCommand
    READ_STATE::Int32
    READ_SETTINGS::Int32
    WRITE_SETTINGS::Int32
    CHANGE_CTRL_STATE::Int32
    NEW_CTRL_POINT::Int32
    __enum_WinchCommand() = new(0,1,2,3,4)
end #type __enum_WinchCommand
const WinchCommand = __enum_WinchCommand()

type __enum_WinchError
    UNDER_VOLTAGE::Int32
    OVER_VOLTAGE::Int32
    OVER_FORCE::Int32
    SYNC_ERROR::Int32
    MAIN_MOTOR::Int32
    SPINDLE_MOTOR::Int32
    MAIN_POS_SENSOR::Int32
    SPINDLE_POS_SENSOR::Int32
    FORCE_SENSOR::Int32
    CELL_OVER_VOLTAGE::Int32
    CELL_UNDER_VOLTAGE::Int32
    __enum_WinchError() = new(0,1,2,3,4,5,6,7,8,9,10)
end #type __enum_WinchError
const WinchError = __enum_WinchError()

type WinchCtrlMix
    v_const_infl::Float32
    f_const_infl::Float32
    v_auto_reelin::Float32
    v_auto_reelout::Float32
    v_auto_reelin_infl::Float32
    v_auto_reelout_infl::Float32
    WinchCtrlMix() = (o=new(); fillunset(o); o)
end #type WinchCtrlMix
meta(t::Type{WinchCtrlMix}) = meta(t, Symbol[:v_const_infl,:f_const_infl], Int[], Dict{Symbol,Any}())

type WinchCtrlPoint
    f_min::Float32
    f_max::Float32
    v_set::Float32
    wind_set::Float32
    fading_time::Float32
    WinchCtrlPoint() = (o=new(); fillunset(o); o)
end #type WinchCtrlPoint
meta(t::Type{WinchCtrlPoint}) = meta(t, Symbol[:f_min,:f_max], Int[], Dict{Symbol,Any}())

type WinchElectrics
    i_main::Float32
    i_spindle::Float32
    i_out::Float32
    i_brake::Float32
    u_batt::Float32
    WinchElectrics() = (o=new(); fillunset(o); o)
end #type WinchElectrics

type WinchSensors
    elevation::Float32
    elevation_offset::Float32
    azimuth_north::Float32
    azimuth_offset::Float32
    der_elevation::Float32
    der_azimuth::Float32
    WinchSensors() = (o=new(); fillunset(o); o)
end #type WinchSensors

type WinchSensorSettings
    zeroElevationVoltage::Float32
    zeroAzimuthVoltage::Float32
    maxElevationVoltage::Float32
    maxAzimuthVoltage::Float32
    WinchSensorSettings() = (o=new(); fillunset(o); o)
end #type WinchSensorSettings

type WinchBattery
    u_cell_max::Float32
    u_cell_min::Float32
    bat_temp_min::Float32
    bat_temp_max::Float32
    bat_charge::Float32
    WinchBattery() = (o=new(); fillunset(o); o)
end #type WinchBattery

type MainSettings
    nom_freq_main::Float32
    max_freq_main::Float32
    max_acc_main::Float32
    motor_inertia_main::Float32
    coulomb_friction_main::Float32
    viscous_friction_main::Float32
    gear_ratio_main::Float32
    encoder_main::Int32
    MainSettings() = (o=new(); fillunset(o); o)
end #type MainSettings
meta(t::Type{MainSettings}) = meta(t, Symbol[], Int[], [:nom_freq_main => 50,:max_freq_main => 95,:max_acc_main => 71.43,:motor_inertia_main => 0.328,:coulomb_friction_main => 122,:viscous_friction_main => 30.6,:gear_ratio_main => 6.2,:encoder_main => 120])

type SpindleSettings
    nom_freq_spindle::Float32
    max_freq_spindle::Float32
    max_acc_spindle::Float32
    motor_inertia_spindle::Float32
    coulomb_friction_spindle::Float32
    viscous_friction_spindle::Float32
    pitch_spindle::Float32
    encoder_spindle::Int32
    SpindleSettings() = (o=new(); fillunset(o); o)
end #type SpindleSettings
meta(t::Type{SpindleSettings}) = meta(t, Symbol[], Int[], [:nom_freq_spindle => 50,:max_freq_spindle => 150,:max_acc_spindle => 100,:motor_inertia_spindle => 0,:coulomb_friction_spindle => 0,:viscous_friction_spindle => 0,:pitch_spindle => 0.97222,:encoder_spindle => 12])

type WinchSettings
    drum_diameter::Float32
    drum_length::Float32
    tether_width::Float32
    tether_height::Float32
    nom_power::Float32
    max_force::Float32
    endpoint_decc::Float32
    force_offset::Float32
    force_factor::Float32
    update_period::Int32
    WinchSettings() = (o=new(); fillunset(o); o)
end #type WinchSettings
meta(t::Type{WinchSettings}) = meta(t, Symbol[], Int[], [:drum_diameter => 0.3,:drum_length => 1,:tether_width => 0.008,:tether_height => 0.002,:nom_power => 18500,:max_force => 3924,:endpoint_decc => 0.5,:force_offset => 0,:force_factor => 250,:update_period => 10])

type WinchState
    counter::Int32
    winch_ctrl::Int32
    winch_mix::WinchCtrlMix
    tether_length_total::Float32
    tether_length_reelout::Float32
    v_reelout::Float32
    force::Float32
    w_elec::WinchElectrics
    w_batt::WinchBattery
    w_sensors::WinchSensors
    ws_settings::WinchSensorSettings
    w_settings::WinchSettings
    m_settings::MainSettings
    s_settings::SpindleSettings
    time::Float64
    time_sent::Float64
    time_received::Float64
    time_clock::Float64
    WinchState() = (o=new(); fillunset(o); o)
end #type WinchState
meta(t::Type{WinchState}) = meta(t, Symbol[:counter,:time], Int[], Dict{Symbol,Any}())

type WinchCtrl
    counter::Int32
    winch_command::Int32
    winch_ctrl::Int32
    winch_point::WinchCtrlPoint
    w_settings::WinchSettings
    ws_settings::WinchSensorSettings
    m_settings::MainSettings
    s_settings::SpindleSettings
    time::Float64
    time_sent::Float64
    time_received::Float64
    crc::Int32
    WinchCtrl() = (o=new(); fillunset(o); o)
end #type WinchCtrl
meta(t::Type{WinchCtrl}) = meta(t, Symbol[:counter,:winch_command,:time], Int[], Dict{Symbol,Any}())

type MiniWinchState
    counter::Int32
    v_reelout::Float32
    tether_length_reelout::Float32
    force::Float32
    u_batt::Float32
    time::Float64
    time_sent::Float64
    MiniWinchState() = (o=new(); fillunset(o); o)
end #type MiniWinchState
meta(t::Type{MiniWinchState}) = meta(t, Symbol[:counter,:v_reelout,:tether_length_reelout,:time_sent], Int[], Dict{Symbol,Any}())

type MiniWinchCtrl
    counter::Int32
    v_set::Float32
    time::Float64
    time_sent::Float64
    MiniWinchCtrl() = (o=new(); fillunset(o); o)
end #type MiniWinchCtrl
meta(t::Type{MiniWinchCtrl}) = meta(t, Symbol[:counter,:time_sent], Int[], Dict{Symbol,Any}())

type WinchLog
    winch_state::WinchState
    winch_ctrl::WinchCtrl
    WinchLog() = (o=new(); fillunset(o); o)
end #type WinchLog

export WinchCtrlState, WinchCommand, WinchError, WinchCtrlMix, WinchCtrlPoint, WinchElectrics, WinchSensors, WinchSensorSettings, WinchBattery, MainSettings, SpindleSettings, WinchSettings, WinchState, WinchCtrl, MiniWinchState, MiniWinchCtrl, WinchLog
export meta
end # module asset_winch
