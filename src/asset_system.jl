module asset_system

using asset_pod
using asset_winch

using ProtoBuf
import ProtoBuf.meta

type __enum_MessageType
    mtProtoMsg::Int32
    mtLogMsg::Int32
    mtBrainyLog::Int32
    mtWinchLog::Int32
    mtEventMsg::Int32
    mtGroundState::Int32
    mtKiteCtrlState::Int32
    mtPodState::Int32
    mtXSenseStream::Int32
    mtPodCtrl::Int32
    mtMiniWinchState::Int32
    mtMiniWinchCtrl::Int32
    mtPilotCtrlState::Int32
    mtEstimatedSystemState::Int32
    mtDesiredTrajectory::Int32
    mtBearing::Int32
    mtAutopilotLog::Int32
    mtStatistics::Int32
    mtCourseControllerSettings::Int32
    mtWinchState::Int32
    mtParticleSystem::Int32
    __enum_MessageType() = new(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21)
end #type __enum_MessageType
const MessageType = __enum_MessageType()

type __enum_SystemState
    ssManualOperation::Int32
    ssParking::Int32
    ssPower::Int32
    ssKiteReelOut::Int32
    ssWaitUntilPointedAtZenith::Int32
    ssDepower::Int32
    ssIntermediate::Int32
    __enum_SystemState() = new(0,1,2,3,4,5,6)
end #type __enum_SystemState
const SystemState = __enum_SystemState()

type __enum_Program
    prgClock::Int32
    prgSensorDaemon::Int32
    prgProtoLogger::Int32
    prgKiteProxy::Int32
    prgSystemStateEstimator::Int32
    prgPodControl::Int32
    prgSystemMonitor::Int32
    prgKiteCtrlDaemon::Int32
    prgCentralControl::Int32
    prgFlightPathController::Int32
    prgAutopilot_2LAP::Int32
    prgAutopilot_3LAP::Int32
    prgFrontView::Int32
    prgTopView::Int32
    prgDataDisplay::Int32
    prgKiteSimulator::Int32
    prgPodMonitor::Int32
    prgLogFileReader::Int32
    __enum_Program() = new(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17)
end #type __enum_Program
const Program = __enum_Program()

type __enum_Event
    START::Int32
    STOP::Int32
    PAUSE::Int32
    CONTINUE::Int32
    RESET::Int32
    CLOSE::Int32
    LAUNCHED::Int32
    LANDED::Int32
    CYCLE_START::Int32
    CYCLE_STOP::Int32
    REELOUT_START::Int32
    REELIN_START::Int32
    UT_START::Int32
    LT_START::Int32
    DEPOWER_STEP::Int32
    DEPOWER_FULL::Int32
    POWER_STEP::Int32
    POWER_FULL::Int32
    NEW_DP_FULL::Int32
    NEW_DP_MIN::Int32
    NEW_ST_FULL::Int32
    KITE_CTRL_CMD::Int32
    SLOW_CLK::Int32
    FAST_CLK::Int32
    POD_TOO_HOT::Int32
    POD_BATT_WEAK::Int32
    WINCH_BATT_WEAK::Int32
    WINCH_TOO_COLD::Int32
    WARN_LATENCY::Int32
    PANIC::Int32
    START_TEST::Int32
    STOP_TEST::Int32
    STEERING_NEUTRAL::Int32
    PARKING_MODE::Int32
    FIG_EIGHT_MODE::Int32
    CIRCLE_MODE::Int32
    START_2LAP::Int32
    STOP_AUTOPILOTS::Int32
    START_NPOINT::Int32
    START_POWER_PRODUCTION::Int32
    FIX_GROUND_POS::Int32
    DEMO_MODE::Int32
    RESET_ESTIMATOR::Int32
    NEW_SYSTEM_STATE::Int32
    NEW_WIND_DIR_OFFSET::Int32
    RESET_MC::Int32
    NEW_STEERING_OFFSET::Int32
    SYNC::Int32
    TRIMBLE_AT_SWIVEL::Int32
    NEW_EST_SETTINGS::Int32
    TEST::Int32
    READY::Int32
    ERROR_RTSIM::Int32
    ERROR_WEAKLINK::Int32
    REPEAT::Int32
    NEW_PROJECT::Int32
    NEW_DEPOWER_OFFSET::Int32
    __enum_Event() = new(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56)
end #type __enum_Event
const Event = __enum_Event()

type __enum_EventSource
    KITE_PILOT::Int32
    KITE_AUTO_PILOT::Int32
    BRAINY::Int32
    WINCH_GUI::Int32
    WINCH_OPERATOR::Int32
    WINCH_HL_CONTROLLER::Int32
    WINCH_LL_CONTROLLER::Int32
    SYSTEM_OPERATOR::Int32
    CLOCK::Int32
    TEST_ENGINE::Int32
    RT_SIM::Int32
    __enum_EventSource() = new(0,1,2,3,4,5,6,7,8,9,10)
end #type __enum_EventSource
const EventSource = __enum_EventSource()

type __enum_CourseControllerType
    CC_PID::Int32
    CC_2LAP::Int32
    CC_PID_II::Int32
    __enum_CourseControllerType() = new(0,1,2)
end #type __enum_CourseControllerType
const CourseControllerType = __enum_CourseControllerType()

type __enum_Condition
    ALWAYS::Int32
    SMALLER::Int32
    BIGGER::Int32
    __enum_Condition() = new(0,1,2)
end #type __enum_Condition
const Condition = __enum_Condition()

type EventMsg
    counter::Int32
    time::Float64
    event::Int32
    event_source::Int32
    period::Float32
    delay::Int32
    flight_id::Int32
    cycle_id::Int32
    steering::Float32
    depower::Float32
    min_depower::Float32
    full_depower::Float32
    full_steering::Float32
    latency::Int32
    winch_temp::Int32
    pod_temp::Int32
    winch_voltage::Float32
    pod_voltage::Float32
    test_no::Int32
    description::String
    cc_type::Int32
    program::Int32
    demo_mode::Int32
    system_state::Int32
    wind_dir_offset::Float32
    steering_offset::Float32
    fast_clk_counter::Int32
    delay_ms::Float32
    checked::Bool
    active_sensors::Int32
    period_time::Float64
    tether_length::Float64
    repeat::Int32
    project::String
    tests::Array{Int32,1}
    depower_offset::Float64
    EventMsg() = (o=new(); fillunset(o); o)
end #type EventMsg
meta(t::Type{EventMsg}) = meta(t, Symbol[:counter,:time,:event,:event_source], Int[], Dict{Symbol,Any}())

type VectorMsg
    counter::Int32
    time::Float64
    value::Array{Float32,1}
    VectorMsg() = (o=new(); fillunset(o); o)
end #type VectorMsg
meta(t::Type{VectorMsg}) = meta(t, Symbol[:counter,:time], Int[], Dict{Symbol,Any}())

type GroundstationPosECEF
    time::Float64
    gpstime::Float64
    latitude::Float32
    longitude::Float32
    altitude::Float32
    GroundstationPosECEF() = (o=new(); fillunset(o); o)
end #type GroundstationPosECEF
meta(t::Type{GroundstationPosECEF}) = meta(t, Symbol[:time], Int[], Dict{Symbol,Any}())

type GroundWind
    time::Float64
    wind_speed_gnd::Float32
    wind_dir_gnd::Float32
    wind_dir_offset::Float32
    GroundWind() = (o=new(); fillunset(o); o)
end #type GroundWind
meta(t::Type{GroundWind}) = meta(t, Symbol[:time], Int[], Dict{Symbol,Any}())

type GroundState
    counter::Int32
    time_sent::Float64
    time_clock::Float64
    wind_gnd::GroundWind
    position_gnd::GroundstationPosECEF
    GroundState() = (o=new(); fillunset(o); o)
end #type GroundState
meta(t::Type{GroundState}) = meta(t, Symbol[:counter,:time_sent], Int[1,2,5,3,4], Dict{Symbol,Any}())

type Statistics
    timestamp::Float64
    counter::Int32
    last_pingtime::Float32
    packages_sent::Int32
    packages_received::Int32
    pingtime_min::Float32
    pingtime_max::Float32
    pingtime_avg::Float32
    pingtest_packagecount::Float32
    bytes_lost::Int32
    chk_error_count::Int32
    rtt_max30::Float32
    rtt_max80::Float32
    packages_later30::Int32
    packages_later80::Int32
    Statistics() = (o=new(); fillunset(o); o)
end #type Statistics
meta(t::Type{Statistics}) = meta(t, Symbol[:timestamp], Int[], Dict{Symbol,Any}())

type PilotCtrlState
    counter::Int32
    rel_depower::Float32
    rel_steering::Float32
    time::Float64
    time_sent::Float64
    PilotCtrlState() = (o=new(); fillunset(o); o)
end #type PilotCtrlState
meta(t::Type{PilotCtrlState}) = meta(t, Symbol[:counter,:time_sent], Int[], Dict{Symbol,Any}())

type WayPoint
    azimuth::Float64
    elevation::Float64
    WayPoint() = (o=new(); fillunset(o); o)
end #type WayPoint
meta(t::Type{WayPoint}) = meta(t, Symbol[:azimuth,:elevation], Int[], Dict{Symbol,Any}())

type SwitchCriterion
    switchpoint::WayPoint
    elevation_criterion::Int32
    azimuth_criterion::Int32
    SwitchCriterion() = (o=new(); fillunset(o); o)
end #type SwitchCriterion
meta(t::Type{SwitchCriterion}) = meta(t, Symbol[:switchpoint], Int[], Dict{Symbol,Any}())

type DesiredTrajectory
    trajectory::Array{WayPoint,1}
    delete_trajectory::Bool
    switch_criterion::Array{SwitchCriterion,1}
    time_sent::Float64
    DesiredTrajectory() = (o=new(); fillunset(o); o)
end #type DesiredTrajectory

type Bearing
    counter::Int32
    bearing::Array{WayPoint,1}
    time_sent::Float64
    Bearing() = (o=new(); fillunset(o); o)
end #type Bearing
meta(t::Type{Bearing}) = meta(t, Symbol[:counter,:time_sent], Int[], Dict{Symbol,Any}())

type StateHeader
    time_rel::Float64
    sys_state::Int32
    test_running::Bool
    test::Int32
    time_sent::Float64
    time_clock::Float64
    StateHeader() = (o=new(); fillunset(o); o)
end #type StateHeader
meta(t::Type{StateHeader}) = meta(t, Symbol[:time_rel], Int[], Dict{Symbol,Any}())

type EstimatedPodState
    header::StateHeader
    state_of_charge::Float32
    steering_difference::Float32
    depower_length::Float32
    steering_force_difference::Float32
    depower_force::Float32
    steering_speed::Float32
    depower_speed::Float32
    mech_steering_power::Float32
    mech_depower_power::Float32
    elec_steering_power::Float32
    elec_depower_power::Float32
    mech_steering_energy::Float32
    mech_depower_energy::Float32
    elec_steering_energy::Float32
    elec_depower_energy::Float32
    tot_mech_steering_energy::Float32
    tot_mech_depower_energy::Float32
    tot_elec_steering_energy::Float32
    tot_elec_depower_energy::Float32
    steering_efficiency::Float32
    depower_efficiency::Float32
    tot_steering_efficiency::Float32
    tot_depower_efficiency::Float32
    u_av_batt::Float32
    u_min_depower::Float32
    u_min_steering::Float32
    EstimatedPodState() = (o=new(); fillunset(o); o)
end #type EstimatedPodState
meta(t::Type{EstimatedPodState}) = meta(t, Symbol[:header], Int[], Dict{Symbol,Any}())

type EstimatedGroundState
    header::StateHeader
    state_of_charge::Float32
    pos::asset_pod.PositionECEF
    mech_power::Float32
    generator_power::Float32
    spindle_power::Float32
    brake_power::Float32
    aux_power::Float32
    mech_energy::Float32
    generator_energy::Float32
    spindle_energy::Float32
    brake_energy::Float32
    aux_energy::Float32
    tot_mech_energy::Float32
    tot_generator_energy::Float32
    tot_spindle_energy::Float32
    tot_brake_energy::Float32
    tot_aux_energy::Float32
    EstimatedGroundState() = (o=new(); fillunset(o); o)
end #type EstimatedGroundState
meta(t::Type{EstimatedGroundState}) = meta(t, Symbol[:header], Int[], Dict{Symbol,Any}())

type Velocity_W
    vx::Float32
    vy::Float32
    vz::Float32
    Velocity_W() = (o=new(); fillunset(o); o)
end #type Velocity_W

type Velocity_EG
    v_east::Float32
    v_north::Float32
    v_up::Float32
    Velocity_EG() = (o=new(); fillunset(o); o)
end #type Velocity_EG

type Force_EG
    f_east::Float32
    f_north::Float32
    f_up::Float32
    Force_EG() = (o=new(); fillunset(o); o)
end #type Force_EG

type Heading_W
    hx::Float32
    hy::Float32
    hz::Float32
    Heading_W() = (o=new(); fillunset(o); o)
end #type Heading_W

type Heading_D
    hx::Float32
    hy::Float32
    Heading_D() = (o=new(); fillunset(o); o)
end #type Heading_D

type Course_D
    cx::Float32
    cy::Float32
    Course_D() = (o=new(); fillunset(o); o)
end #type Course_D

type PositionEG
    pos_east::Float32
    pos_north::Float32
    height::Float32
    PositionEG() = (o=new(); fillunset(o); o)
end #type PositionEG

type PositionEAKD
    el_pos::Float32
    az_pos::Float32
    kite_distance::Float32
    PositionEAKD() = (o=new(); fillunset(o); o)
end #type PositionEAKD

type EstimatedSystemState
    header::StateHeader
    pod_state::EstimatedPodState
    ground_state::EstimatedGroundState
    pos_east::Float32
    pos_north::Float32
    height::Float32
    elevation::Float32
    azimuth::Float32
    kite_distance::Float32
    tether_length_reelout::Float32
    heading::Float32
    course::Float32
    avg_windvelocity_ground::Float32
    avg_winddirection_ground::Float32
    windvelocity_kite_altitude::Float32
    winddirection_kite_altitude::Float32
    apparent_windvelocity_kite::Float32
    velocity_w::Velocity_W
    heading_w::Heading_W
    heading_d::Heading_D
    course_d::Course_D
    is_trimble_used::Bool
    pos_old::PositionEAKD
    pos_new::PositionEAKD
    pos_ang::PositionEAKD
    app_windvelocity_kite::Float32
    EstimatedSystemState() = (o=new(); fillunset(o); o)
end #type EstimatedSystemState
meta(t::Type{EstimatedSystemState}) = meta(t, Symbol[:header], Int[], Dict{Symbol,Any}())

type LogMsg
    pod_state::asset_pod.PodState
    pod_ctrl::asset_pod.PodCtrl
    winch_state::asset_winch.WinchState
    winch_ctrl::asset_winch.WinchCtrl
    event::EventMsg
    ground_state::GroundState
    statistics::Statistics
    LogMsg() = (o=new(); fillunset(o); o)
end #type LogMsg

type AutopilotLog
    time::Float64
    counter::Int32
    cmptime::Float64
    issuing_counter::Int32
    bearing::Float64
    heading::Float64
    ctrl_signal::Float64
    nue_baseline::Float64
    crosstrack::Float64
    optparam::Float64
    nue_b_dyn::Float64
    nue_adapt::Float64
    error_adapt::Float64
    parameters::Array{Float64,1}
    AutopilotLog() = (o=new(); fillunset(o); o)
end #type AutopilotLog
meta(t::Type{AutopilotLog}) = meta(t, Symbol[:time,:counter], Int[1,2,11,10,3,4,5,6,12,13,14,7,8,9], Dict{Symbol,Any}())

type ProtoMsg
    time_received::Float64
    time_rel::Float64
    log_msg::LogMsg
    brainy_log::asset_pod.BrainyLog
    winch_log::asset_winch.WinchLog
    event_msg::EventMsg
    ground_state::GroundState
    pod_state::asset_pod.PodState
    kite_ctrl_state::asset_pod.KiteCtrlState
    pod_ctrl::asset_pod.PodCtrl
    pilot_ctrl_state::PilotCtrlState
    mini_winch_state::asset_winch.MiniWinchState
    est_system_state::EstimatedSystemState
    autopilot_log::AutopilotLog
    statistics::Statistics
    desired_trajectory::DesiredTrajectory
    bearing::Bearing
    winch_state::asset_winch.WinchState
    ProtoMsg() = (o=new(); fillunset(o); o)
end #type ProtoMsg

type Settings3LAP
    time::Float64
    counter::Int32
    p_gain::Float64
    i_gain::Float64
    i_limit::Float64
    delta_zero::Float64
    t_zero::Float64
    t_zero_lp_freq::Float32
    adaption::Bool
    adaption_gain_1::Float64
    adaption_gain_2::Float64
    adaption_gain_3::Float64
    adaption_gain_4::Float64
    adaption_sigma::Float64
    adaption_lp_freq::Float64
    adaption_bound_1::Float64
    adaption_bound_2::Float64
    adaption_bound_3::Float64
    adaption_bound_4::Float64
    use_course::Bool
    max_steering::Float64
    bearing_lp_freq::Float64
    inv_param_c1::Float64
    inv_param_c2::Float64
    inv_param_c3::Float64
    inv_param_c4::Float64
    Settings3LAP() = (o=new(); fillunset(o); o)
end #type Settings3LAP
meta(t::Type{Settings3LAP}) = meta(t, Symbol[:time,:counter], Int[1,2,3,4,5,6,7,40,8,9,10,11,12,20,21,22,23,24,25,33,34,35,36,37,38,39], Dict{Symbol,Any}())

type CourseControllerSetting
    depower::Float64
    pGain::Float64
    iGain::Float64
    dGain::Float64
    max_steering::Float64
    CourseControllerSetting() = (o=new(); fillunset(o); o)
end #type CourseControllerSetting

type CourseControllerSettings
    counter::Int32
    time::Float64
    course_controller_settings::Array{CourseControllerSetting,1}
    period_time::Float64
    CourseControllerSettings() = (o=new(); fillunset(o); o)
end #type CourseControllerSettings
meta(t::Type{CourseControllerSettings}) = meta(t, Symbol[:counter,:time], Int[], Dict{Symbol,Any}())

type ParticleSystem
    time::Float64
    counter::Float64
    positions::Array{PositionEG,1}
    v_reel_out::Float64
    l_tether::Float64
    force::Float64
    v_wind_ground::Float64
    height::Float64
    v_wind_height::Float64
    v_app_x::Float64
    v_app_y::Float64
    v_app_z::Float64
    rel_steering::Float64
    rel_depower::Float64
    azimuth::Float64
    elevation::Float64
    velocities::Array{Velocity_EG,1}
    pos_predicted::Array{PositionEG,1}
    forces::Array{Force_EG,1}
    ParticleSystem() = (o=new(); fillunset(o); o)
end #type ParticleSystem
meta(t::Type{ParticleSystem}) = meta(t, Symbol[:time,:counter], Int[], Dict{Symbol,Any}())

export MessageType, SystemState, Program, EventSource, CourseControllerType, Condition, EventMsg, VectorMsg, GroundstationPosECEF, GroundWind, GroundState, Statistics, PilotCtrlState, WayPoint, SwitchCriterion, DesiredTrajectory, Bearing, StateHeader, EstimatedPodState, EstimatedGroundState, Velocity_W, Velocity_EG, Force_EG, Heading_W, Heading_D, Course_D, PositionEG, PositionEAKD, EstimatedSystemState, LogMsg, AutopilotLog, ProtoMsg, Settings3LAP, CourseControllerSetting, CourseControllerSettings, ParticleSystem
export meta
end # module asset_system
