This repository contains text and data for testing Julia with Google protocoll buffer
encoded messages. Currently implemented:

- sending messages via a ZeroMQ link

Not yet implemented:

- reading log files like those in the log folder with Julia

It is using the nice library from:
https://github.com/tanmaykm/ProtoBuf.jl

Uwe Fechner, 9.3.2014
